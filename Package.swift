// swift-tools-version:3.1

import PackageDescription

let package = Package(
    name: "w3code",
    
    dependencies: [
        .Package(url: "https://github.com/PerfectlySoft/Perfect-HTTPServer.git", majorVersion: 2),
        .Package(url: "https://github.com/SwiftORM/MySQL-StORM.git", majorVersion: 1),
    ]
)
