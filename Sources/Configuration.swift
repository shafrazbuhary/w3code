//
//  Configuration.swift
//  w3code
//
//  Created by Mohamed Shafraz on 8/27/17.
//
//

import Foundation

open class Configuration {
    
    private var info = [String:String]()
    
    public init(contentsOf file: String) {
        
        let lines = try! String(contentsOfFile: file).components(separatedBy: "\n")
        
        
        for line in lines {
            //Escape comments
            guard !line.hasPrefix("//") && line.trim.lengh > 0 else {
                continue
            }
            
            let item = line.components(separatedBy: ":")
            info[item[0].trim] = item[1].trim
        }
        
    }
    
    public func getValue(key: String) -> String? {
        return info[key]
    }
    
}
