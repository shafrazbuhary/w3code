//
//  Data.swift
//  w3code
//
//  Created by Mohamed Shafraz on 8/16/17.
//
//

import Foundation

public enum JSONError: Error {
    case CannotPass(description: String)
}

extension Data {
    
    public func toString() -> String? {
        return String(data: self, encoding: .utf8)
    }
    
    public var jsonObject: Any? {
        do {
            return try jsonObject()
        } catch {
            return nil
        }
    }
    
    public func jsonObject(options opt: JSONSerialization.ReadingOptions = []) throws -> Any {
        do {
            return try JSONSerialization.jsonObject(with:self, options:opt)
        } catch let error {
            throw JSONError.CannotPass(description: error.localizedDescription)
        }
    }
    
}
