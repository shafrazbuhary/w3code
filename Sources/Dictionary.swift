//
//  Dictionary.swift
//  w3code
//
//  Created by Mohamed Shafraz on 8/17/17.
//
//

import Foundation

extension Dictionary {
    public var jsonString: String? {
        do {
            let data = try JSONSerialization.data(withJSONObject: self, options: .prettyPrinted)
            return data.toString()
        } catch {
            return nil
        }
    }
}
