//
//  HandlesGetRequests.swift
//  w3code
//
//  Created by Mohamed Shafraz on 7/30/17.
//
//

import PerfectHTTP
import PerfectHTTPServer

public protocol HTTPRequestHandler {}

public protocol HandlesGetRequest: HTTPRequestHandler {
    public func didReceiveGetRequest(request:HTTPRequest, response : HTTPResponse)
}

public protocol HandlesHeadRequest: HTTPRequestHandler {
    public func didReceiveHeadRequest(request:HTTPRequest, response : HTTPResponse)
}

public protocol HandlesPostRequest: HTTPRequestHandler {
    public func didReceivePostRequest(request:HTTPRequest, response : HTTPResponse)
}

public protocol HandlesPatchRequest: HTTPRequestHandler {
    public func didReceivePatchRequest(request:HTTPRequest, response : HTTPResponse)
}

public protocol HandlesPutRequest: HTTPRequestHandler {
    public func didReceivePutRequest(request:HTTPRequest, response : HTTPResponse)
}

public protocol HandlesDeleteRequest: HTTPRequestHandler {
    public func didReceiveDeleteRequest(request:HTTPRequest, response : HTTPResponse)
}

extension HandlesGetRequest {
    public var getHendler : RequestHandler {
        return {
            request, response in
            self.didReceiveGetRequest(request: request, response: response)
        }
    }
}

extension HandlesHeadRequest {
    public var headHendler : RequestHandler {
        return {
            request, response in
            self.didReceiveHeadRequest(request: request, response: response)
        }
    }
}


extension HandlesPostRequest {
    public var postHendler : RequestHandler {
        return {
            request, response in
            self.didReceivePostRequest(request: request, response: response)
        }
    }
}

extension HandlesPatchRequest {
    public var patchHendler : RequestHandler {
        return {
            request, response in
            self.didReceivePatchRequest(request: request, response: response)
        }
    }
}

extension HandlesPutRequest {
    public var putHendler : RequestHandler {
        return {
            request, response in
            self.didReceivePutRequest(request: request, response: response)
        }
    }
}

extension HandlesDeleteRequest {
    public var deleteHendler : RequestHandler {
        return {
            request, response in
            self.didReceiveDeleteRequest(request: request, response: response)
        }
    }
}
