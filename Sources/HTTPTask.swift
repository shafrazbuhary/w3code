//
//  HTTPTask.swift
//  w3code
//
//  Created by Mohamed Shafraz on 8/2/17.
//
//

import Foundation
import PerfectHTTP

public typealias HTTPTaskHandler = (HTTPURLResponse?,Data?,Error?) -> Void

open class HTTPTask {
    
    public var request: URLRequest
    public var sessionConfiguration = URLSessionConfiguration.default
    public var handler: HTTPTaskHandler?
    
    public init(request: URLRequest, handler: @escaping HTTPTaskHandler) {
        self.request = request
        self.handler = handler
    }
    
    public init(urlString: String, method: HTTPMethod, handler: @escaping HTTPTaskHandler) throws {
        guard let encodedUrl = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed),
            let url = URL(string:encodedUrl) else {
                throw URLError(.badURL)
        }
        
        request = URLRequest(url:url)
        request.httpMethod = method.description
        self.handler = handler
    }
    
    public func run() {
        let urlSession = URLSession(configuration: sessionConfiguration)
        let sessionTask = urlSession.dataTask(with: request) { (data, response, error) in
            self.handler?(response as? HTTPURLResponse,data,error)
        }
        
        sessionTask.resume()
    }
    
}
