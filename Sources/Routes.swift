//
//  Routes.swift
//  w3code
//
//  Created by Mohamed Shafraz on 8/27/17.
//
//

import Foundation

import PerfectLib
import PerfectHTTP
import PerfectHTTPServer

extension Routes {
    
    public mutating func get(uri:String,handler: @escaping RequestHandler)  {
        self.add(method: .get, uri: uri, handler: handler)
    }
    
    public mutating func post(uri:String,handler: @escaping RequestHandler)  {
        self.add(method: .post, uri: uri, handler: handler)
    }
    
    public mutating func head(uri:String, handler: @escaping RequestHandler) {
        self.add(method: .head, uri: uri, handler: handler)
    }
    
    public mutating func patch(uri:String, handler: @escaping RequestHandler)  {
        self.add(method: .patch, uri: uri, handler: handler)
    }
    
    public mutating func put(uri:String, handler: @escaping RequestHandler)  {
        self.add(method: .put, uri: uri, handler: handler)
    }
    
    public mutating func delete(uri:String, handler: @escaping RequestHandler)  {
        self.add(method: .delete, uri: uri, handler: handler)
    }
    
    
    public mutating func add(uri:String, handlers: [(method: HTTPMethod, handler: RequestHandler)]) {
        for (method , handler) in handlers {
            self.add(method: method, uri: uri, handler: handler)
        }
    }
    
    public mutating func add(uri:String, httpHandler: HTTPRequestHandler) {
        
        if let handler = httpHandler as? HandlesGetRequest {
            self.add(method: .get, uri: uri, handler: handler.getHendler)
        }
        
        if let handler = httpHandler as? HandlesHeadRequest {
            self.add(method: .head, uri: uri, handler: handler.headHendler)
        }
        
        if let handler = httpHandler as? HandlesPostRequest {
            self.add(method: .post, uri: uri, handler: handler.postHendler)
        }
        
        if let handler = httpHandler as? HandlesPatchRequest {
            self.add(method: .patch, uri: uri, handler: handler.patchHendler)
        }
        
        if let handler = httpHandler as? HandlesPutRequest {
            self.add(method: .put, uri: uri, handler: handler.putHendler)
        }
        
        if let handler = httpHandler as? HandlesDeleteRequest {
            self.add(method: .delete, uri: uri, handler: handler.deleteHendler)
        }
    }
}
