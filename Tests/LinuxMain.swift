import XCTest
@testable import w3codeTests

XCTMain([
    testCase(w3codeTests.allTests),
])
