import XCTest
@testable import w3code

class w3codeTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        XCTAssertEqual(w3code().text, "Hello, World!")
    }


    static var allTests = [
        ("testExample", testExample),
    ]
}
